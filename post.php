<?php
require("./pdo/pdo.php");
require("classes/blog.class.php");

$page_id = $_GET['page'];
if(isset($page_id) && $page_id > 0){
    $oBlog = new blog($bdd); // oBlog object init

    $article = $oBlog->getPost($page_id) ; // get the post
    $req_com = $oBlog->getComments($page_id); // get comments
    $req_add_com = $oBlog->AddComment($page_id); // add comment

    // check if the url image is set
    if ($article['img']){
        $img = $article['img'];
    }
    else{$img = "./img/FFFFFF.png" ;}

    require("vue/post_vue.php"); // get the post vue
}

else{ echo "<h3 classes='articles-title'>Cet article n'existe pas</h3>";}

