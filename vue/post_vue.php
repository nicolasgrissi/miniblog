<?php $title = $article['titre']; ?>

<?php ob_start(); //enclenche la temporisation de sortie ?>

<div class="wrapper">
	<div class="articles">
		<div><a href="./" class="btn btn-back">Revenir aux articles</a></div>
		<br>
		<br><?php
        echo "<div class='article-single-box' ><img style='max-width: 100% !important; height: auto !important;'  src='" . $img . "' >";
        echo "<h2 class='articles-title-single'> " . $article['titre'] . "</h2>";
        echo "<small class='articles-date info' >&#9719; Ecrit le " . $article['date_creation'] . "</small>";
        echo "<p class='articles-contenu'>" . nl2br($article['contenu']) . "</p></div>";
        ?>
	</div>
	<div class="message">
		<h3 class="commentaire-titre">Commentaires</h3><?php
        while ($commentaire = $req_com->fetch()) {
            echo "<div class='comment-box'>";
            echo "<p class='commentaire-auteur'><strong>" . nl2br(htmlspecialchars($commentaire['auteur'])) . "</strong></p>";
            echo "<small class='articles-date'>Ecrit le " . $commentaire['date_commentaire'] . "</small>";
            echo "<p class='articles-contenu comment-content'>" . nl2br(htmlspecialchars($commentaire['commentaire'])) . "</p>";
            echo "</div>";
        }
        $req_com->closeCursor(); ?>
		<div class="form">
			<h4 class="commentaire-titre commentaire-form info">Ajouter un commentaire</h4>

			<form action="./post.php?page=<?php echo $page_id ?>" method="post">
				<label for="name">Nom </label><br><input type="text" name="name" id="" required>
				<br>
				<label for="content">
					Commentaire </label><br><textarea name="content" id="" cols="30" rows="1" required></textarea>
				<br>
				<br>
				<button class="btn btn-new" type="submit">Poster</button>
			</form>
		</div>
	</div>

</div>

<?php $content = ob_get_clean(); // lit le contenu courant du tampon de sortie puis l'efface ?>

<?php require "./template/template.php"; ?>








