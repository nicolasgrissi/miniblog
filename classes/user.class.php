<?php

class user
{
    private $bdd;

    /**
     * admin constructor.
     * INIT PDO OBJECT
     * @param $bdd
     */
    public function __construct($bdd)
    {
        $this->bdd = $bdd;
    }

    /**
     * ADD NEW USER IN DATABASE
     */
    public function AddUser()
    {
        $req_checkuser = $this->bdd->prepare("SELECT * FROM users WHERE name = :login");
        $req_checkuser->execute(array(
            'login' => $_POST['login']
        ));
        $user = $req_checkuser->fetch();

        if ($user == null) {

            $req_adduser = $this->bdd->prepare("INSERT INTO users (name,password,date) VALUES (:addname , :addpass, NOW())");
            $password = password_hash($_POST['password'], PASSWORD_BCRYPT);
            $req_adduser->execute(array(
                'addname' => $_POST['login'],
                'addpass' => $password,
                /*NOW() est déja inseré dans la preparation SQL */
            ));
            $_SESSION['message']['success'] = 'Vous êtes maintenant inscrit, vous pouvez vous connecter avec vos identifiants';
            $req_adduser->closeCursor();
            header('location:login.php');
            exit();


        }
        else { $_SESSION['message']['exist'] = 'Ce nom d\'utilisateur existe déjà , choisisez en un autre' ;}
    }

    /**
     * CHECK IF USER EXIT
     */
    public function CheckUser()
    {
        session_start();
        $req_checkuser = $this->bdd->prepare("SELECT * FROM users WHERE name = :login");
        $req_checkuser->execute(array(
            'login' => $_POST['login']
        ));
        $user = $req_checkuser->fetch();
        if ($user == null) {
            $_SESSION['message']['denied'] = 'Erreur ! Login et/ou Mot de passe incorrect, réessayez';

        } elseif (password_verify($_POST['password'], $user['password'])) {
            $_SESSION['auth'] = $user;

            header('location:admin.php');
            exit();
        } else {
            $_SESSION['message']['denied'] = 'Erreur ! Login et/ou Mot de passe incorrect, réessayez';
        }

    }




}