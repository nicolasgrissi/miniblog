<?php

class blog
{

    private $bdd;
    public $page;

    /**
     * blog constructor.
     * init pdo object
     * @param $bdd
     */
    public function __construct($bdd)
    {
        $this->bdd = $bdd;
    }

    /**
     * get ALL the posts
     * @return object , All posts
     */
    public function getPosts()
    {
        $nb_article_par_page = 5; // variable contenant le total des articles voulus par page.
        $req_all = $this->bdd->query("SELECT COUNT(*) AS nb_articles FROM articles"); // on compte de nb d'articles
        // au total
        $count_art = $req_all->fetch();
        $countart = $count_art['nb_articles']; // on stocke le nb d'article total dans une variable ( retourne une seule info)
        // On détermine le nombre de pages :
        $nb_pages = ceil($countart / $nb_article_par_page);

        // Maintenant on va afficher les messages :
        if (isset($_GET['page'])) {
            if ($_GET['page'] < 100) {
                $page = (int)$_GET['page']; // on récupère le numéro de la page indiqué dans l'adresse.
            } else {
                header('Location:index.php');
            }
        } else {
            $page = 1; // page par défaut si la variable n'existe pas.
        }

        // On calcule le numéro du premier article qu'on prend pour le LIMIT :
        $premierArticleAafficher = ($page - 1) * $nb_article_par_page;

        // On ferme la requête avant d'en faire une autre :
        $req_all->closeCursor();
        $rqt_all = null;

        /*------------------pagination FIN-------------------*/

        if(isset($premierArticleAafficher) && isset($nb_article_par_page))
        {
            //requete avec pagination
            $req_home = $this->bdd->query("SELECT id,titre,img, DATE_FORMAT(date_creation, '%d/%m/%Y à %Hh%i') AS date_creation
        FROM articles  ORDER BY id DESC LIMIT " . $premierArticleAafficher . ',' . $nb_article_par_page);
            $this->page = $page;
            $this->nb_pages = $nb_pages;


            return $req_home;
        } else {
            $req_home = $this->bdd->query("SELECT id,titre,img, DATE_FORMAT(date_creation, '%d/%m/%Y à %Hh%i') AS
            date_creation FROM articles  ORDER BY id DESC");
            return $req_home;
        }

    }

    /**
     * generate HTML of pagination
     * @return  echo pagination
     */
    public function getPagination()
    {
        for($i = 1; $i <= $this->nb_pages ; $i++ ){

            if($i == $this->page)
            {
                echo '<a href="index?page=' . $this->page . '" class="pagination-current">' . $this->page . '</a> ';
            } else {
            echo '<a href="index?page=' . $i . '">' . $i . '</a> ';
            }
        }

    }


    /**
     *
     * get the content of the post
     * @param $page_id
     * @return mixed
     */
    public function getPost($page_id)
    {


        $req_post = $this->bdd->prepare("SELECT id,titre,contenu,img, DATE_FORMAT(date_creation, '%d/%m/%Y à %Hh%i') AS date_creation FROM articles WHERE id = :page_id ");
        $req_post->execute();
        $article = $req_post->fetch();
        return $article;

    }


    /**
     *
     * get all comments of the post
     * @param $page_id
     * @return mixed, ALL comments array
     */
    public function getComments($page_id)
    {

        $req_com = $this->bdd->prepare("SELECT id_article,auteur,commentaire, DATE_FORMAT(date_commentaire, '%d/%m/%Y à %Hh%i') AS date_commentaire FROM commentaires 
        WHERE id_article = :article ORDER BY date_commentaire DESC");
        $req_com->execute(array(
            'article' => $page_id));
        return $req_com;

    }

    /**
     *
     * get all comments of the post
     * @param $page_id
     * @return mixed, ALL comments array
     */
    public function AddComment($page_id)
    {

        if (isset($_POST["name"]) AND isset($_POST["content"])) {
            $name = $_POST["name"];
            $comm = $_POST["content"];


            $req_add_com = $this->bdd->prepare('INSERT INTO commentaires(id_article,auteur ,commentaire,date_commentaire) VALUES (:id_art,:aut, :comm,  NOW())');

            $req_add_com->execute(array(
                'id_art' => $page_id,
                'aut' => $name,
                'comm' => $comm,
                /*NOW() est déja inseré dans la preparation SQL */
            ));


            $req_add_com->closeCursor(); // Termine le traitement de la requête

            header('location:post.php?page=' . $page_id);


        }

    }

}
