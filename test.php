<?php
/**
 * Created by PhpStorm.
 * User: nicolas.grissi
 * Date: 31/08/2018
 * Time: 09:37
 */
try {
    $bdd = new PDO('mysql:host=localhost;dbname=test;charset=utf8', 'root', '',
        array
        (PDO::ATTR_ERRMODE
        => PDO::ERRMODE_EXCEPTION)); // tester la connexion à la bdd
} catch (Exception $e) {
    die('Erreur : ' . $e->getMessage()); // si ratée affichée la connexion à la bdd
}

require("./classes/blog.class.php");

$oBlog = new blog($bdd);
$oAdmin = new admin($bdd);

$result = $oBlog->getPost(1);
$oAdmin->test(12);

echo '<pre>';
print_r($result);
echo '<pre>';


